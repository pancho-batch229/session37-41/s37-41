const jwt = require("jsonwebtoken");

// [Section] JSON Web Tokens
/*
- JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
- Information is kept secure through the use of the secret code
- Only the system that knows the secret code that can decode the encrypted information

- Imagine JWT as a gift wrapping service that secures the gift with a lock
- Only the person who knows the secret code can open the lock
- And if the wrapper has been tampered with, JWT also recognizes this and disregards the gift
- This ensures that the data is secure from the sender to the receiver
*/

// Token Creation

const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {})
}


// Token Verification 
/*

	Analogy:
		Receive the gift and open it to verify if the sender is legit and gift wasn't tampered.

*/

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	console.log(token);

	if(typeof token !== "undefined") {
		console.log(token);

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {
			// IF JWT ISNT VALID
			if(error) {
				return res.send({auth: "failed"}) 
			} else {
				// TO PROCEED TO THE NEXT MIDDLEWARE FUNCTION || CALLBACK IN THE ROUTE
				next();
			}
		});
	} else {
		return res.send({auth: "failed"})
	}
}

// TOKEN DECRYPTION	
/*

	Analogy:
		Open the gift and get the content.

*/

module.exports.decode = (token) => {
	if(typeof token !== "undefined") {
		// removes the "bearer" prefix
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {
			if(error) {
				return null;
			} else {
				// decode method - used to obtain info from the jwt
				// complete: true - option allows to return additional info from the jwt token
				// payload contains info provided in the "createAccess"
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	} else {
		return null;
	}
}

// hi :)