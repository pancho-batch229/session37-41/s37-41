const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers.js");
const auth = require("../auth")

//create course
router.post('/', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	courseControllers.addCourse(req.body, req.user)
		.then(resultFromController => res.send(resultFromController))
});

// Retrieve all courses 
router.get("/all", (req, res) => {
	courseController.getAllCourses()
		.then(resultFromController => res.send(resultFromController))
})



/*

	MINI ACTIVITY
	1. Crete a route that will retrieve all active courses (endpoint: "/")
	2. No need for user login
	3. Create a controller that will return all active courses
	4. send your postman output sc to our batch hangouts

*/



// Retrieve All Active Courses
router.get('/active-courses', (req, res) => {
	courseController.getAllActiveCourse()
		.then(resultFromController => res.send(resultFromController))
})


// Retrieve Specific Active Courses
			// wild card and req.params
router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params)
		.then(resultFromController => res.send(resultFromController))

})


// update course

// only admins can update courses
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body)
		.then(resultFromController => res.send(resultFromController))

})


// Archiving Course
router.put("/:courseId/archive", auth.verify, (req, res) => {
	const data = {
		reqParams: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.archiveCourse(data).then(resultFromController => res.send(resultFromController))
})

module.exports = router;