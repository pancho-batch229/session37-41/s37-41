const Course = require("../models/Course.js");
const authenticate = require("../auth.js");

/*

	Create New course

	steps: 
		1. Create new course object using the mongoose model
		2. save the new course to the database

*/

module.exports.addCourse = (reqBody) => {
	// created new variable "newCourse" and instantiate a new course object
	// uses information from the req body to provide all neccessary info
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	// saves the created object to the database
	return newCourse.save().then((course, error) => {
		// course creation failed
		if (error){
			return false;
			// course creation succeeded
		} else {
			return true;
		}
	});
}

// retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result
	})
}

// get all active courses
module.exports.getAllActiveCourse = () => {
	return Course.find({ isActive: true }).then(result => {
		return result
	})
}


// get specified courses
module.exports.getCourse = (reqParams) => {
	console.log(reqParams);
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// update course
/*

	Steps: 
	 1. create a variable "updatedCourse" which will contain the info retrieved from the req body.
	 2. Find and update the course using the course ID retrieved from the req params and the vairable "updatedCourse".

	
*/


module.exports.updateCourse = (reqParams, reqBody) => {
	// specify the fields of the document to be updated
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

// 	findByIdUpdate(document ID(wildcard), updatedCourse) finds Id then updates - it has 2 parameters
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		// 									   wildcard
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}


// archive course
module.exports.archiveCourse = (data) => {
	if(data.isAdmin === true) {
		let updateActiveField = {
			isActive:false
		}

		return Course.findByIdAndUpdate(data.reqParams.courseId, updateActiveField).then((course, error) => {
			if(error) {
				return false;
			} else {
				return true;
			}
		});
	} else {

	}
}